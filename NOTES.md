- Test right now either..
  1. Returns a umsg and prints it..but continues to hang. Addr is correct though!
  2. Seg faults
  3. Hangs without reading the umsg
- Something's off...
- Linked with -optl-pthread seems necessary??? weird
- https://lwn.net/Articles/786896/

IDEAS:
- Use poll(2) like the example
  - In general, copy the example more literally
- Do this in an executable instead of test suite to remove indirection

Bug log:
```
      -- TODO: Why does this -1 when the "hi" above is commented out??
      {-
msg = UFFD_MSG_Pagefault {uffd_msg_flags = 5144096, uffd_msg_address = 28}
Input data to handle pagefault: d
errno = invalid argument (Invalid argument)
-- ^ Where is this coming from? What are those flags (non-0..) and what is that addr?)
```
^ I'm pretty sure this happened when I used alloca for the ptr in the handler. The ptr
type was Word8, so it would only really allocate 1 byte. Maybe there was some race w/GHC's
allocator? Since I was also allocating elsewhere? Changing it to allocaBytes uffd_msg_size
stopped the error from occurring. It's unclear if this is actually the issue, but it makes
sense + fixed it.
