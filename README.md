```
$ cabal new-run test1 2
[From ThreadId 8] Input data to handle pagefault: One green thread is reading C strings from virtual memory.
[From ThreadId 7] PAGE 0 = One thread is reading C strings from virtual memory.
[From ThreadId 8] Input data to handle pagefault: Another is handling them by asking stdin.
[From ThreadId 7] PAGE 1 = Another is handling them by asking stdin.
```
