{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE RecordWildCards #-}

module Main where

import Data.Bits
import System.Exit (die)
import System.Environment (lookupEnv, getArgs)
import Foreign.Marshal.Array (peekArray0, pokeArray0)
import Control.Concurrent
import GHC.Conc
import Foreign.C.Error (getErrno, errnoToIOError)
import System.Posix.IO (fdReadBuf)
import Foreign.Marshal.Utils (with)
import Foreign.C.Types (CInt)
import Control.Exception (throwIO)
import Foreign.Ptr (ptrToWordPtr, plusPtr)
import Text.Read (readMaybe)
import Foreign.Marshal.Alloc (allocaBytes)
import Data.Foldable(for_)
import System.IO
import Control.Monad (forever)

import Userfaultfd
import Userfaultfd.C.Ioctl
import Userfaultfd.C.Userfaultfd
import Userfaultfd.C.Mmap

import qualified System.Posix.Thread as P

main :: IO ()
main = do
  hSetBuffering stdout NoBuffering
  num_pages <- getArgs >>= \case
    [n] -> maybe (error "num_pages not a number") pure $ readMaybe n
    _ -> die $ "Please pass a single number for num_pages"

  vlog $ "rtsSupportsBoundThreads = " ++ show rtsSupportsBoundThreads
  P.myThreadId >>= \x -> vlog $ "main pthread id = " ++ show x
  vlog $ "numCapabilities = " ++ show numCapabilities
  isCurrentThreadBound >>= \b -> vlog $ "main bound = " ++ show b
  
  ufd <- userfaultfd (o_cloexec .|. 0)
  vlog $ "ufd = " ++ show ufd
  api_ret <- with default_uffdio_api_argp $ ioctl ufd cmd_UFFDIO_API
  checkErrno api_ret

  anona (fromIntegral $ page_size * num_pages) $ \mptr -> do
    let maddr = fromIntegral $ ptrToWordPtr mptr
        range = UFFDIO_Range
          { uffdio_range_start = maddr
          , uffdio_range_len = fromIntegral $ page_size * num_pages
          }
        register = UFFDIO_REGISTER_argp
          { uffdio_register_range = range
          , uffdio_register_mode = uffdio_register_mode_missing
          , uffdio_register_ioctls = 0
          }

    vlog $ "maddr = " ++ show maddr
    register_ret <- with register $ ioctl ufd cmd_UFFDIO_REGISTER
    checkErrno register_ret

    -- Spawn a thread to read from the ufd. It will receive data once
    -- any page in REGISTERed range is touched
    --
    -- We can use forkIO here as all we're doing is reading a file, making a
    -- safe FFI call & checking errno (which is part of the TSO. See
    -- https://stackoverflow.com/a/14553484)
    _ <- forkIO $ pagea $ \page -> allocaBytes uffd_msg_size $ \ptr -> forever $ do
      numRead <- fdReadBuf ufd ptr uffd_msg_size
      vlog $ "numRead = " ++ show numRead
      faultAddr <- peekUFFD_MSG ptr >>= \case
        x@UFFD_MSG_Pagefault{..} -> vlog ("msg = " ++ show x) >> pure uffd_msg_address
        x -> die $ "UNEXPECTED UFFD_MSG: " ++ show x
      info "Input data to handle pagefault: "
      s <- getLine
      pokeArray0 '\NUL' page s
      let copyArgs = UFFDIO_COPY_argp
            { uffdio_copy_src = fromIntegral $ ptrToWordPtr page
            , uffdio_copy_dst = faultAddr
            , uffdio_copy_len = page_size
            , uffdio_copy_mode = 0
            , uffdio_copy_copy = 0
            }
      copy_ret <- with copyArgs $ ioctl ufd cmd_UFFDIO_COPY
      checkErrno copy_ret

    -- Read all the pages in order
    allocaBytes page_size $ \page ->
      for_ [0..num_pages-1] $ \page_num -> do
        _ <- safe_memcpy page (mptr `plusPtr` (page_num * page_size)) page_size
        peekedStr <- peekArray0 '\NUL' page
        infoln $ "PAGE " ++ show page_num ++ " = " ++ peekedStr


-----------------------------------------------------------------------------

shouldBe :: Eq a => Show a => a -> a -> IO ()
shouldBe x y = if x == y then pure () else error $ show x ++ " NOT = " ++ show y

checkErrno :: CInt -> IO ()
checkErrno = \case
  0 -> pure ()
  _ -> do
    e <- getErrno
    throwIO $ errnoToIOError "" e Nothing Nothing

wait1 :: IO ()
wait1 = threadDelay 1000000

infoln :: String -> IO ()
infoln s = myThreadId >>= \tid -> putStrLn $ "[From " ++ show tid ++ "] " ++ s

info :: String -> IO ()
info s = myThreadId >>= \tid -> putStr $ "[From " ++ show tid ++ "] " ++ s

vlog :: String -> IO ()
vlog s = lookupEnv "VERBOSE" >>= \case
  Nothing -> pure ()
  Just _ -> putStrLn s

pfence :: String -> IO a -> IO a
pfence tag ioa = do
  tid <- myThreadId
  pid <- P.myThreadId
  isBound <- isCurrentThreadBound
  vlog $ "|T=" ++ show tid ++ "|P=" ++ show pid ++ "|B=" ++ show isBound ++ "|" ++ tag ++ " START"
  a <- ioa
  vlog $ "|T=" ++ show tid ++ "|P=" ++ show pid ++ "|B=" ++ show isBound ++ "|" ++ tag ++ " END"
  pure a
