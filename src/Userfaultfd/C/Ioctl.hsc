{-# LANGUAGE StrictData #-}
{-# LANGUAGE RecordWildCards #-}

module Userfaultfd.C.Ioctl where

import Foreign.Storable
import Data.Word
import Data.Int
import Foreign.C.Types
import Foreign.Ptr
import System.Posix.Types

#include <sys/ioctl.h>
#include <linux/userfaultfd.h>

-- http://man7.org/linux/man-pages/man2/ioctl_userfaultfd.2.html

foreign import ccall unsafe "sys/ioctl.h ioctl" ioctl
  :: Fd -- fd
  -> CInt -- cmd
  -> Ptr a -- argp (depends on cmd)
  -> IO CInt

-- | ioctl errno codes
-- TODO: #include
{-
efault :: CInt
efault = #{const EFAULT}

einval :: CInt
einval = #{const EINVAL}

ebusy :: CInt
ebusy = #{const EBUSY}

eagain :: CInt
eagain = #{const EAGAIN}

enoent :: CInt
enoent = #{const ENOENT}

enospc :: CInt
enospc = #{const ENOSPC}

esrch :: CInt
esrch = #{const ESRCH}
-}
-- TODO: Singleton types for cmds

cmd_UFFDIO_API :: CInt
cmd_UFFDIO_API = #{const UFFDIO_API}

data UFFDIO_API_argp = UFFDIO_API_argp
  { uffdio_api_api :: Word64
  , uffdio_api_features :: Word64
  , uffdio_api_ioctls :: Word64
  } deriving (Eq, Show)

-- instance Storable UFFDIO_API where

default_uffdio_api_argp :: UFFDIO_API_argp
default_uffdio_api_argp =
  UFFDIO_API_argp
    { uffdio_api_api = uffd_api
    , uffdio_api_features = uffd_features_none
    , uffdio_api_ioctls = uffd_ioctls_default
    }

uffd_api :: Word64
uffd_api = #{const UFFD_API}

uffd_features_none :: Word64
uffd_features_none = 0

uffd_ioctls_default :: Word64
uffd_ioctls_default = 0

cmd_UFFDIO_REGISTER :: CInt
cmd_UFFDIO_REGISTER = #{const UFFDIO_REGISTER}

data UFFDIO_REGISTER_argp = UFFDIO_REGISTER_argp
  { uffdio_register_range :: UFFDIO_Range
  , uffdio_register_mode :: Word64
  , uffdio_register_ioctls :: Word64
  } deriving (Eq, Show)

data UFFDIO_Range = UFFDIO_Range
  { uffdio_range_start :: Word64
  , uffdio_range_len :: Word64
  } deriving (Eq, Show)

uffdio_register_mode_missing :: Word64
uffdio_register_mode_missing = #{const UFFDIO_REGISTER_MODE_MISSING}

-- NOTE: "Currently, the only supported mode is UFFDIO_REGISTER_MODE_MISSING."

uffdio_register_mode_wp :: Word64
uffdio_register_mode_wp = #{const UFFDIO_REGISTER_MODE_WP}

cmd_UFFDIO_UNREGISTER :: CInt
cmd_UFFDIO_UNREGISTER = #{const UFFDIO_UNREGISTER}
-- NOTE: UFFDIO_UNREGISTER's argp is UFFDIO_Range

cmd_UFFDIO_COPY :: CInt
cmd_UFFDIO_COPY = #{const UFFDIO_COPY}

data UFFDIO_COPY_argp = UFFDIO_COPY_argp
  { uffdio_copy_dst :: Word64
  , uffdio_copy_src :: Word64
  , uffdio_copy_len :: Word64
  , uffdio_copy_mode :: Word64
  , uffdio_copy_copy :: Int64
  }

uffdio_copy_mode_dontwake :: Word64
uffdio_copy_mode_dontwake = #{const UFFDIO_COPY_MODE_DONTWAKE}

cmd_UFFDIO_ZEROPAGE :: CInt
cmd_UFFDIO_ZEROPAGE = #{const UFFDIO_ZEROPAGE}

data UFFDIO_ZEROPAGE_argp = UFFDIO_ZEROPAGE_argp
  { uffdio_zeropage_range :: UFFDIO_Range
  , uffdio_zeropage_mode :: Word64
  , uffdio_zeropage_zeropage :: Int64
  }

uffdio_zeropage_mode_dontwake :: Word64
uffdio_zeropage_mode_dontwake = #{const UFFDIO_ZEROPAGE_MODE_DONTWAKE}

cmd_UFFDIO_WAKE :: CInt
cmd_UFFDIO_WAKE = #{const UFFDIO_WAKE}

-- NOTE: UFFDIO_WAKE's argp is UFFDIO_Range

-- Storable instances
instance Storable UFFDIO_API_argp where
  alignment _ = #{alignment struct uffdio_api}
  sizeOf _ = #{size struct uffdio_api}

  peek p = do
    uffdio_api_api <- #{peek struct uffdio_api, api} p
    uffdio_api_features <- #{peek struct uffdio_api, features} p
    uffdio_api_ioctls <- #{peek struct uffdio_api, ioctls} p
    pure UFFDIO_API_argp{..}

  poke p UFFDIO_API_argp{..} = do
    #{poke struct uffdio_api, api} p uffdio_api_api
    #{poke struct uffdio_api, features} p uffdio_api_features
    #{poke struct uffdio_api, ioctls} p uffdio_api_ioctls

instance Storable UFFDIO_REGISTER_argp where
  alignment _ = #{alignment struct uffdio_register}
  sizeOf _ = #{size struct uffdio_register}

  peek p = do
    uffdio_register_range <- #{peek struct uffdio_register, range} p
    uffdio_register_mode <- #{peek struct uffdio_register, mode} p
    uffdio_register_ioctls <- #{peek struct uffdio_register, ioctls} p
    pure UFFDIO_REGISTER_argp{..}

  poke p UFFDIO_REGISTER_argp{..} = do
    #{poke struct uffdio_register, range} p uffdio_register_range
    #{poke struct uffdio_register, mode} p uffdio_register_mode
    #{poke struct uffdio_register, ioctls} p uffdio_register_ioctls

instance Storable UFFDIO_Range where
  alignment _ = #{alignment struct uffdio_range}
  sizeOf _ = #{size struct uffdio_range}

  peek p = do
    uffdio_range_start <- #{peek struct uffdio_range, start} p
    uffdio_range_len <- #{peek struct uffdio_range, len} p
    pure UFFDIO_Range{..}

  poke p UFFDIO_Range{..} = do
    #{poke struct uffdio_range, start} p uffdio_range_start
    #{poke struct uffdio_range, len} p uffdio_range_len

instance Storable UFFDIO_COPY_argp where
  alignment _ = #{alignment struct uffdio_copy}
  sizeOf _ = #{size struct uffdio_copy}

  peek p = do
    uffdio_copy_dst <- #{peek struct uffdio_copy, dst} p
    uffdio_copy_src <- #{peek struct uffdio_copy, src} p
    uffdio_copy_len <- #{peek struct uffdio_copy, len} p
    uffdio_copy_mode <- #{peek struct uffdio_copy, mode} p
    uffdio_copy_copy <- #{peek struct uffdio_copy, mode} p
    pure UFFDIO_COPY_argp{..}

  poke p UFFDIO_COPY_argp{..} = do
    #{poke struct uffdio_copy, dst} p uffdio_copy_dst
    #{poke struct uffdio_copy, src} p uffdio_copy_src
    #{poke struct uffdio_copy, len} p uffdio_copy_len
    #{poke struct uffdio_copy, mode} p uffdio_copy_mode
    #{poke struct uffdio_copy, copy} p uffdio_copy_copy
