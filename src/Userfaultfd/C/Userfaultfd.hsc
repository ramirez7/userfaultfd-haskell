{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE RecordWildCards #-}

module Userfaultfd.C.Userfaultfd where

import Data.Word
import Foreign.C.Types
import System.Posix.Types
import Foreign.Ptr
import Foreign.Storable

#include <sys/types.h>
#include <fcntl.h>
#include <linux/userfaultfd.h>
#include <sys/syscall.h>

foreign import ccall unsafe "sys/syscall syscall" syscall
  :: CLong
  -> CLong
  -> IO CLong

nr_userfaultfd :: CLong
nr_userfaultfd = #{const __NR_userfaultfd}

userfaultfd :: CInt -> IO Fd
userfaultfd flags = fromIntegral <$> syscall nr_userfaultfd (fromIntegral flags)

o_cloexec :: CInt
o_cloexec = #{const O_CLOEXEC}

o_nonblock :: CInt
o_nonblock = #{const O_NONBLOCK}

data UFFD_MSG =
    UFFD_MSG_Pagefault
      { uffd_msg_flags :: Word64
      , uffd_msg_address :: Word64
      }
  | UFFD_MSG_Fork
      { uffd_msg_ufd :: Word32 }
  | UFFD_MSG_Remap
      { uffd_msg_from :: Word64
      , uffd_msg_to :: Word64
      , uffd_msg_len :: Word64
      }
  | UFFD_MSG_Remove
      { uffd_msg_start :: Word64
      , uffd_msg_end :: Word64
      }
  deriving (Eq, Show)

uffd_msg_size :: Integral a => a
uffd_msg_size = #{size struct uffd_msg}

-- | We don't provide a 'Storable' instance for 'UFFD_MSG' because
-- there's only ever a need to 'peek' it.
peekUFFD_MSG :: Ptr a -> IO UFFD_MSG
peekUFFD_MSG p = do
  (event :: Word8) <- #{peek struct uffd_msg, event} p
  case event of
    #{const UFFD_EVENT_PAGEFAULT} -> do
      uffd_msg_flags <- #{peek struct uffd_msg, arg.pagefault.flags} p
      uffd_msg_address <- #{peek struct uffd_msg, arg.pagefault.address} p
      pure UFFD_MSG_Pagefault{..}
    #{const UFFD_EVENT_FORK} -> do
      uffd_msg_ufd <- #{peek struct uffd_msg, arg.fork.ufd} p
      pure UFFD_MSG_Fork{..}
    #{const UFFD_EVENT_REMAP} -> do
      uffd_msg_from <- #{peek struct uffd_msg, arg.remap.from} p
      uffd_msg_to <- #{peek struct uffd_msg, arg.remap.to} p
      uffd_msg_len <- #{peek struct uffd_msg, arg.remap.len} p
      pure UFFD_MSG_Remap{..}
    #{const UFFD_EVENT_REMOVE} -> peek_UFFD_MSG_Remove
    #{const UFFD_EVENT_UNMAP} -> peek_UFFD_MSG_Remove
    _ -> error $ "Unrecognized uffd_msg.event: " ++ show event
  where
    peek_UFFD_MSG_Remove = do
      uffd_msg_start <- #{peek struct uffd_msg, arg.remove.start} p
      uffd_msg_end <- #{peek struct uffd_msg, arg.remove.end} p
      pure UFFD_MSG_Remove{..}
