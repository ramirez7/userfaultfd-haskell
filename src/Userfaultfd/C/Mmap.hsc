module Userfaultfd.C.Mmap where

import Foreign.C.Types
import Foreign.Ptr
import System.Posix.Types

#include <sys/mman.h>
#include <sys/types.h>
#include <linux/userfaultfd.h>
#include <string.h>
#include <unistd.h>

foreign import ccall safe "sys/mman.h mmap" mmap
  :: Ptr () -- Starting address
  -> CSize -- Length
  -> CInt -- prot
  -> CInt -- flags
  -> Fd
  -> CLong
  -> IO (Ptr a)

foreign import ccall safe "sys/mman.h munmap" munmap
  :: Ptr a
  -> CSize
  -> IO ()

-- | 'Foreign.Marshal.Utils.copyBytes' does not work because it is @unsafe@ -
-- it freezes when the range is REGISTERed.
foreign import ccall safe "string.h memcpy" safe_memcpy
  :: Ptr a -- dest
  -> Ptr a -- src
  -> CSize -- n
  -> IO (Ptr a) -- also dest

-- prot
protExec :: CInt
protExec = #{const PROT_EXEC}

protRead :: CInt
protRead = #{const PROT_READ}

protWrite :: CInt
protWrite = #{const PROT_WRITE}

protNone :: CInt
protNone = #{const PROT_NONE}

-- flags
-- base
mapPrivate :: CInt
mapPrivate = #{const MAP_PRIVATE}

-- options
mapFixed :: CInt
mapFixed = #{const MAP_FIXED}

mapAnonymous :: CInt
mapAnonymous = #{const MAP_ANONYMOUS}

--

foreign import ccall unsafe "unistd.h sysconf" sysconf :: CInt -> CLong

page_size :: Integral a => a
page_size = fromIntegral $ sysconf #{const _SC_PAGE_SIZE}
