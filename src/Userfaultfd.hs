{-# LANGUAGE ExplicitForAll #-}
{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Userfaultfd where

import Data.Bits
import Foreign.Ptr
import Foreign.C.Types
import System.Posix.Types (Fd)
import Control.Exception (bracket)
import Foreign.Storable (Storable, poke, peek, sizeOf)
import Foreign.Marshal.Alloc (alloca)

import Userfaultfd.C.Mmap

withMMap
  :: Ptr () -- Starting address
  -> CSize -- Length
  -> CInt -- prot
  -> CInt -- flags
  -> Fd
  -> CLong
  -> (Ptr a -> IO b)
  -> IO b
withMMap addr len prot flags fd offset =
  bracket
  (mmap addr len prot flags fd offset)
  (\ptr -> munmap ptr len)

withSmallMMap :: (Ptr a -> IO b) -> IO b
withSmallMMap = withMMap nullPtr 10000000 (protRead .|. protWrite) (mapPrivate .|. mapAnonymous) (-1) 0

smallMMap :: IO (Ptr a)
smallMMap = mmap nullPtr 10000000 (protRead .|. protWrite) (mapPrivate .|. mapAnonymous) (-1) 0

anona :: CSize -> (Ptr a -> IO b) -> IO b
anona len = withMMap nullPtr len (protRead .|. protWrite) (mapPrivate .|. mapAnonymous) (-1) 0

pagea :: (Ptr a -> IO b) -> IO b
pagea = anona page_size

readWriteWord :: Ptr CLong -> IO ()
readWriteWord ptr = do
  putStrLn "about to peek1"
  x1 <- peek ptr
  putStrLn $ "x1 = " ++ show x1

  poke ptr 24

  x2 <- peek ptr
  putStrLn $ "x2 = " ++ show x2

-- 'peek', but first perform a 'safe_memcpy' from the given 'Ptr'
-- into a GHC-allocated 'Ptr'. This will be well-behaved in the
-- face of userfaultfd at the cost of performing an extra copy.
--
-- TODO: Add a 'newtype' with a 'Storable' instance that does this
-- under the hood
safe_peek :: forall a. Storable a => Ptr a -> IO a
safe_peek ptr = alloca $ \dest -> do
  _ <- safe_memcpy dest ptr (csizeOf @a)
  peek dest

csizeOf :: forall a. Storable a => CSize
csizeOf = fromIntegral $ sizeOf (undefined :: a)
