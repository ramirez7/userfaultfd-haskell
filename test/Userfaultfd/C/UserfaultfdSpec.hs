module Userfaultfd.C.UserfaultfdSpec where

import           Test.Hspec

import Data.Bits
import Control.Concurrent
import System.Timeout (timeout)
import GHC.Conc
import Foreign.C.Types(CLong)
import System.Posix.IO (fdReadBuf)
import Foreign.Marshal.Utils (with)
import Foreign.Ptr (nullPtr, ptrToWordPtr)
import Foreign.Marshal.Alloc (alloca)
import Foreign.Storable (peek)

import Control.Monad (forever)

import Userfaultfd
import Userfaultfd.C.Ioctl
import Userfaultfd.C.Userfaultfd
import Userfaultfd.C.Mmap

import System.Posix.Thread as P

spec :: Spec
spec = do
  describe "userfaultfd" $ do
    it "should WORK" $ runInBoundThread $ do
      putStrLn $ "rtsSupportsBoundThreads = " ++ show rtsSupportsBoundThreads
      P.myThreadId >>= \x -> putStrLn $ "main1 pthread id = " ++ show x
      putStrLn $ "numCapabilities = " ++ show numCapabilities
      isCurrentThreadBound >>= \b -> putStrLn $ "spec bound = " ++ show b
      ufd <- userfaultfd 0
      putStrLn $ "ufd = " ++ show ufd
      api_ret <- with default_uffdio_api_argp $ ioctl ufd cmd_UFFDIO_API
      putStrLn $ "api_ret = " ++ show api_ret
      api_ret `shouldBe` 0
      let mapSize = 4096 * 5
          mapProt = protRead .|. protWrite
          mapFlags = mapPrivate .|. mapAnonymous
      withMMap nullPtr mapSize mapProt mapFlags (-1) 0 $ \mptr -> do
        let maddr = fromIntegral $ ptrToWordPtr mptr
            range = UFFDIO_Range
              { uffdio_range_start = maddr
              , uffdio_range_len = 4096 * 5
              }
            register = UFFDIO_REGISTER_argp
              { uffdio_register_range = range
              , uffdio_register_mode = uffdio_register_mode_missing
              , uffdio_register_ioctls = 0
              }
        putStrLn  $ "maddr = " ++ show maddr
        register_ret <- with register $ ioctl ufd cmd_UFFDIO_REGISTER
        register_ret `shouldBe` 0
        putStrLn "hello11!"
        forkOS $ forever $ do
          P.myThreadId >>= \x -> putStrLn $ "forever pthread id = " ++ show x
          putStrLn "forever.."
          threadDelay 1000000

        P.myThreadId >>= \x -> putStrLn $ "main2 pthread id = " ++ show x


        forkOS $ alloca $ \ptr -> do
          putStrLn $ "TEST!"
          P.myThreadId >>= \x -> putStrLn $ "umsg pthread id = " ++ show x
          numRead <- fdReadBuf ufd ptr uffd_msg_size
          putStrLn $ "numRead = " ++ show numRead
          umsg <- peekUFFD_MSG ptr
          putStrLn $ "umsg = " ++ show umsg
          
        forkOS $ do
          P.myThreadId >>= \x -> putStrLn $ "rw pthread id = " ++ show x
          isCurrentThreadBound >>= \b -> putStrLn $ "rw bound = " ++ show b
--          x <- peek mptr :: IO CLong
--          putStrLn $ "peeked x = " ++ show x

        putStrLn "hello!"

        threadDelay 1000000

        pure ()
