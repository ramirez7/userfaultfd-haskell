{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}

module Userfaultfd.C.IoctlSpec where

import           Test.Hspec
import           Test.Hspec.QuickCheck
import           Test.QuickCheck
import           Test.QuickCheck.Instances ()

import Foreign.Marshal.Alloc
import Data.Typeable
import Data.Proxy
import Foreign.Storable

import Userfaultfd.C.Ioctl

spec :: Spec
spec = do
  describe "Storable Instances" $ do
    storableRoundtrip gen_UFFDIO_API_argp
    storableRoundtrip gen_UFFDIO_Range
    storableRoundtrip gen_UFFDIO_REGISTER_argp

ffor :: Functor f => f a -> (a -> b) -> f b
ffor = flip fmap

-- This generates _more_ values than are valid arguments
gen_UFFDIO_API_argp :: Gen UFFDIO_API_argp
gen_UFFDIO_API_argp =
  UFFDIO_API_argp <$> arbitrary <*> arbitrary <*> arbitrary

gen_UFFDIO_REGISTER_argp :: Gen UFFDIO_REGISTER_argp
gen_UFFDIO_REGISTER_argp =
  UFFDIO_REGISTER_argp <$> gen_UFFDIO_Range <*> arbitrary <*> arbitrary

gen_UFFDIO_Range :: Gen UFFDIO_Range
gen_UFFDIO_Range =
  UFFDIO_Range <$> arbitrary <*> arbitrary

-- This is generally useful :)
storableRoundtrip
  :: forall a
   . Typeable a
  => Storable a
  => Eq a
  => Show a
  => Gen a
  -> Spec
storableRoundtrip genA =
  prop ("Roundtrip " ++ show (typeRep $ Proxy @a)) $
    ffor genA $ \a -> ioProperty $ alloca $ \ptr -> do
      poke ptr a
      rt <- peek ptr
      rt `shouldBe` a
